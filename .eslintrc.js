module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/recommended',
        '@vue/airbnb',
    ],
    parserOptions: {
        parser: 'babel-eslint',
    },
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-param-reassign': ['error', { props: false }],
        'comma-dangle': [
            'error',
            'always-multiline',
        ],
        'import/prefer-default-export': 'off',
        'func-names': 'off',
        'prefer-destructuring': 'warn',
        'no-restricted-globals': ['off', 'location', 'top'],
        'no-plusplus': 'off',
        'consistent-return': 'off',
        'no-new': 'off',
        'object-curly-newline': ['error', {
            ObjectExpression: { consistent: true },
        }],
        'max-len': ['warn', 150],
        indent: ['error', 4],
        'vue/html-indent': ['error', 5],
    },
};
