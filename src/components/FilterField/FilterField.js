import { mapGetters, mapActions } from 'vuex';

const data = function () {
    return {
        innerValue: '',
    };
};

const computed = {
    ...mapGetters('covid', [
        'covidResults',
    ]),
    countries() {
        return this.covidResults.map((el) => el.Country);
    },
};

const methods = {
    ...mapActions('covid', [
        'saveFilterTerm',
        'filterCovidResults',
    ]),
    onTextFieldInput() {
        const value = this.innerValue;
        this.saveFilterTerm(value);
        this.filterCovidResults(value);
    },
};

const FilterField = {
    data,
    computed,
    methods,
};

export default FilterField;
