import { Bar, mixins } from 'vue-chartjs';

const { reactiveProp } = mixins;

const props = {
    options: {
        type: Object,
        default: null,
    },
};

const mounted = function () {
    // this.chartData is created in the mixin.
    this.renderChart(this.chartData, this.options);
};

const BarChart = {
    extends: Bar,
    mixins: [reactiveProp],
    props,
    mounted,
};

export default BarChart;
