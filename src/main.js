import Vue from 'vue';

import './styles/main.scss';

import App from './App.vue';
import router from './router';
import { makeStore } from './store';
import './plugins';

Vue.config.productionTip = false;

new Vue({
    router,
    store: makeStore(),
    render: (h) => h(App),
}).$mount('#app');
