import Vue from 'vue';
import VueRouter from 'vue-router';
import TableViewComponent from '../views/TableView/Table.view.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'TableViewComponent',
        component: TableViewComponent,
    },
    {
        path: '/chart',
        name: 'ChartViewComponent',
        component: () => import(/* webpackChunkName: "ChartViewComponent" */ '../views/ChartView/Chart.view.vue'),
    },
];

const router = new VueRouter({
    base: process.env.BASE_URL,
    routes,
});

export default router;
