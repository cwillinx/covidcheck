import { mapGetters } from 'vuex';

const data = function () {
    return {
        fields: [
            {
                key: 'Country',
                sortable: true,
            },
            {
                key: 'TotalConfirmed',
                label: 'Total Active',
                sortable: true,
                formatter: (value) => value.toLocaleString('en-US'),
            },
            {
                key: 'TotalDeaths',
                sortable: true,
                formatter: (value) => value.toLocaleString('en-US'),
            },
            {
                key: 'TotalRecovered',
                sortable: true,
                formatter: (value) => value.toLocaleString('en-US'),
            },
        ],
    };
};

const computed = {
    ...mapGetters('covid', [
        'filteredCovidResults',
    ]),
};

const TableView = {
    data,
    computed,
};

export default TableView;
