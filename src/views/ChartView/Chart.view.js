import { mapGetters } from 'vuex';

import BarChart from '../../components/BarChart/BarChart.vue';

const components = {
    BarChart,
};

const data = function () {
    const fontColor = '#212529';
    const fontFamily = 'Source Sans Pro';

    return {
        options: {
            responsive: true,
            maintainAspectRatio: false,
            defaultFontFamily: fontFamily,
            defaultFontSize: 14,
            defaultFontColor: fontColor,
            dataset: {
                barPercentage: 0.8,
                barThickness: 'flex',
                categoryPercentage: 1,
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        color: '#eee',
                    },
                    ticks: {
                        fontFamily,
                        fontSize: 14,
                        fontColor,
                        lineWidth: 0,
                    },
                }],
                yAxes: [{
                    gridLines: {
                        display: false,
                        color: '#eee',
                    },
                    ticks: {
                        fontFamily,
                        fontSize: 12,
                        fontColor,
                        beginAtZero: false,
                    },
                }],
            },
            legend: {
                display: true,
                labels: {
                    fontFamily,
                    fontSize: 14,
                    fontColor,
                },
            },
            tooltips: {
                backgroundColor: '#FFF',
                bodyFontFamily: fontFamily,
                titleFontFamily: fontFamily,
                footerFontFamily: fontFamily,
                bodyFontColor: fontColor,
                titleFontColor: fontColor,
                footerFontColor: fontColor,
                borderColor: fontColor,
                borderWidth: 1,
            },
        },
    };
};

const computed = {
    ...mapGetters('covid', [
        'covidDataLoaded',
        'covidResults',
        'filteredCovidResults',
    ]),
    datacollection() {
        const colors = [
            '#ff3355',
            '#666',
            '#20c997',
        ];

        const filteredResults = this.filteredCovidResults.slice(0, -1);

        const labels = filteredResults.map((el) => el.Country);
        const datasets = [];

        datasets.push({
            label: 'Total Active',
            backgroundColor: colors[0],
            data,
        });
        datasets[0].data = filteredResults.map((el) => el.TotalConfirmed);

        datasets.push({
            label: 'Total Deaths',
            backgroundColor: colors[1],
            data,
        });
        datasets[1].data = filteredResults.map((el) => el.TotalDeaths);

        datasets.push({
            label: 'Total Recovered',
            backgroundColor: colors[2],
            data,
        });
        datasets[2].data = filteredResults.map((el) => el.TotalRecovered);

        return {
            labels,
            datasets,
        };
    },
};

const ChartView = {
    components,
    data,
    computed,
};

export default ChartView;
