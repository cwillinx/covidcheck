import Vue from 'vue';
import Vuex from 'vuex';

import covid from './modules/covid';

Vue.use(Vuex);

const modules = {
    covid,
};

const storeOptions = {
    strict: true,
    modules,
};

const makeStore = function () {
    return new Vuex.Store(storeOptions);
};

export { makeStore };
