import Vue from 'vue';

import { getSummaryFromCovidAPI, getSummaryFromCovidAPIMocked } from '../../services/covid';
import { TOTAL_ROW_NAME } from '../../services/constants';

const initialState = function () {
    return {
        covidResults: [],
        filteredCovidResults: [],
        filterTerm: '',
    };
};

const getters = {
    covidResults: ({ covidResults }) => covidResults,
    filteredCovidResults: ({ filteredCovidResults }) => filteredCovidResults,
    filterTerm: ({ filterTerm }) => filterTerm,
    covidDataLoaded: ({ covidResults }) => covidResults && covidResults.length,
};

const mutations = {
    setCovidResults(state, results) {
        Vue.set(state, 'covidResults', results);
    },
    setFilteredCovidResults(state, results) {
        Vue.set(state, 'filteredCovidResults', results);
    },
    setFilterTerm(state, term) {
        Vue.set(state, 'filterTerm', term);
    },
};

const mapCountries = ({
    Country,
    TotalConfirmed,
    TotalDeaths,
    TotalRecovered,
}) => ({
    Country,
    TotalConfirmed,
    TotalDeaths,
    TotalRecovered,
});

const actions = {
    async fetchCovidResults({ commit, dispatch }) {
        try {
            const response = await getSummaryFromCovidAPIMocked();
            if (response.status && response.message) {
                throw response.message;
            }
            const mappedResults = response.Countries.map(mapCountries);
            commit('setCovidResults', mappedResults);
            dispatch('filterCovidResults', '');
        } catch (error) {
            // eslint-disable-next-line no-console
            console.error('Covid Store Module Error: fetchCovidResults()', error);
        }
    },
    async filterCovidResults({ commit, getters: localGetters }, filter) {
        const completeData = localGetters.covidResults;
        const filterLowerCase = filter.toLowerCase();

        const filteredData = completeData.filter((el) => el.Country.toLowerCase().includes(filterLowerCase));

        const reducer = (acc, curr) => ({
            TotalConfirmed: acc.TotalConfirmed + curr.TotalConfirmed,
            TotalDeaths: acc.TotalDeaths + curr.TotalDeaths,
            TotalRecovered: acc.TotalRecovered + curr.TotalRecovered,
        });

        const totals = filteredData.reduce(
            reducer,
            {
                TotalConfirmed: 0,
                TotalDeaths: 0,
                TotalRecovered: 0,
            },
        );

        totals.Country = TOTAL_ROW_NAME;

        filteredData.push(totals);

        commit('setFilteredCovidResults', filteredData);
    },
    saveFilterTerm({ commit }, filter) {
        commit('setFilterTerm', filter);
    },
};

const covidModule = {
    namespaced: true,
    state: initialState,
    mutations,
    actions,
    getters,
};

export default covidModule;
