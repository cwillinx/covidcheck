import { mapActions } from 'vuex';

import AppHeader from './components/AppHeader/AppHeader.vue';
import NavBar from './components/NavBar/NavBar.vue';
import FilterField from './components/FilterField/FilterField.vue';

const components = {
    AppHeader,
    NavBar,
    FilterField,
};

const methods = {
    ...mapActions('covid', ['fetchCovidResults']),
};

const created = function () {
    this.fetchCovidResults();
};

const AppComponent = {
    components,
    methods,
    created,
};

export default AppComponent;
